/* JSON Menubars
    Copyright 2022 Targren.com - Used under license    
 */


//Every Navigation should have a MenuRoot div with the appropriate ID (that is, the 'menuId' value used in the json file)
//and it should usually be empty (though not necessarily always), and have at least the CSS class "NavMenu" (again, not necessarily always)


//# Global Variables - Change these to suit
var JsonPath = "/menujson/";


//Make sure that the vital bits are present in the calling page before trying to load the menu.
//You can add more checks here, if needed
document.addEventListener('DOMContentLoaded', function (){
    if (window.jQuery){
        if (typeof menuInit === 'function'){
            menuInit();
        } else {
            alert("Could not find menuInit function");
        }
    } else {
        alert("JQuery Not Loaded");
    }
});



//### No edits required below this line for normal usage

function buildPermsObject(phpPermString){
    let newObj = {};
    phpPermString.split(",").forEach(function (p){
        newObj[p] = true;
    });
    return newObj;
}


function menuInit() {
    let jsonfile = JsonPath + "nav_" + menuName + ".json";

    fetch(jsonfile)
        .then(response => {
            if (!response.ok) {
                switch (response.status) {
                    case 404:
                        if (typeof menuName === "undefined" || menuName === "") {
                            //Setup Error: This should be set in the X_navigation.php file in a <script> header
                            throw new Error("Javascript Error: No 'menuName' variable set.");
                        } else {
                            //Setup Error: Did you remember to create the menu definition?
                            throw new Error("Javascript Error: Could not find nav_" + menuName + ".json file in " + JsonPath);
                        }
                        break;
                    case 403:
                        //Setup Error: Check your filesystem permissions
                        throw new Error("Javascript Error: Could not open menu definition. Permission denied.");
                        break;
                    case 500:
                        //Look in apache's error.log
                        throw new Error("Javascript Error: Server Error attempting to retrieve menu definition.");
                        break;
                    default:
                        //Start at apache's error.log and go from there. Good luck!
                        throw new Error("Javascript Error: HTTP Error " + response.status + " when attempting to retrieve menu definition.");
                }
            }
            return response;
        })
        .then(response => {
            let json = response.json();
            return json;
        })
        .then(data => {
            buildMenu(data);

            if (typeof postInit == 'function'){
                postInit();
            }
        })
        .catch(err => {
                //None of these files should error
                let msg = err.message;
                console.log(err); //To see the line/backtrace
                alert(msg);
            }
        );
}


function buildMenu(menuData){
    let menuRoot = document.getElementById(menuData['menuId']);
    if (menuRoot == null){
        throw new Error("HTML Error: Could not locate the menu root element for " + menuRoot);
    }
    menuData['menuItems'].forEach(item =>{
		//Permissions need to be populated in the navbar html <script>
        if (item.permissions != null && item.permissions.perms != null){
            if (item.permissions.perms.adminOnly != null){
                return (userPermissions.is_admin === true);
            } else {
                let flag = false;
                if (item.permissions.perms.length > 0) {
                    if (item.permissions.grouping != null && item.permissions.grouping === "all" && item.permissions.perms.length > 1) {
                        flag = true;
                        for (const p in item.permissions.perms){
                            let perm =  item.permissions.perms[p];
                            if (userPermissions[perm] == null && userPermissions["is_admin"] == null){
                                flag = false;
                                break;
                            }
                        }
                    } else { //Grouping is either missing, or 'any' (or some bogus value), or there's only 1 permission. Treat them all the same way
                        flag = false;
                        //Check for single string permissions
                        if (typeof(item.permissions.perms) === "string"){
                            flag = userPermissions[item.permissions.perms];
                        } else {
                            for (const p in item.permissions.perms) {
                                let perm = item.permissions.perms[p];
                                if (userPermissions[perm] || userPermissions["is_admin"]) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!flag){ return; }
                }
            }
        }

        if (item['itemId'] != null){
            if ( item['label'] == null ||
                 item['target'] == null ||
                (item['url'] == null && item['callback'] == null)
            ){ alert("JSON Error: Required properties missing from Menu Item definition. See log for details." );
                console.error("JSON Error: Required properties missing from Menu Item definition");
                console.error(item);
                return;
            }

            addMenuItem(menuRoot, item, false);
        } else if (item['formId'] != null){
            if ( item['inputs'] == null ||
                (item['url'] == null && item['submitCallback'] == null)
            ){ alert("JSON Error: Required properties missing from Menu Input definition. See log for details." );
                console.error("JSON Error: Required properties missing from Menu Input definition");
                console.error(item);
                return;
            }
            addMenuForm(menuRoot, item);
        } else if (item['bannerId'] != null){
            if (false){ //[NYI]
                { alert("JSON Error: Required properties missing from Menu Banner definition. See log for details." );
                    console.error("JSON Error: Required properties missing from Menu Banner definition");
                    console.error(item);
                    return;
                }
            }
            addMenuBanner(menuRoot, item);
        } else if (item['spacerId'] != null) {
            addMenuSpacer(menuRoot, item);
        } else {
            alert("JSON Error: Invalid entry. See log for details.");
            console.error("Warning: No valid ID field found for passed menu component");
            console.error(item);
        }
    });
}

function addMenuItem(menuRoot, itemData){
    let cssClass = "NavItem  NavDiv";

    if (itemData['cssClasses'] != null){
        itemData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }
    let newItem = document.createElement('div');
    newItem.id = itemData["itemId"];
    newItem.className = cssClass;
    newItem.textContent = itemData["label"];
    newItem.url = itemData['url'];
    newItem.target = itemData['target'];
    newItem.query = "";
    if (itemData['query'] != null){
        if (typeof itemData['query']['string'] != 'undefined'){
            newItem.query = itemData['query']["string"];
        }
        if ((typeof itemData['query']['vars'] != 'undefined') && itemData["query"]["vars"].length > 0){
            let varary = [];
            itemData["query"]["vars"].forEach(qvar =>{
                if (typeof qvar.index !== "undefined"){
                    varary.push(encodeURIComponent(qvar.name) + "=" + encodeURIComponent(passVars[qvar.index]));
                } else {
                    varary.push(encodeURIComponent(qvar.name) + "=" + encodeURIComponent(qvar.value));
                }


            });
            newItem.query = "?" + varary.join("&");
        }
    }
    if (itemData.data != null){
        newItem.data = itemData.data;
    }


    //Assign events
    newItem.addEventListener('click', MenuClick, false);
    newItem.addEventListener('mouseover', HoverOn, false);
    newItem.addEventListener('mouseout', HoverOff, false);


    menuRoot.appendChild(newItem);
}

function addMenuForm(menuRoot, itemData){

    let newItem = document.createElement("div");
    newItem.id = "div" + itemData['formId'];
    newItem.className = "NavFormDiv";
    let formRoot = document.createElement("form");
    formRoot.id=itemData['formId'];
    formRoot.className="NavForm";
    formRoot.target=itemData['target'];
    formRoot.action = itemData['url'];

    itemData['inputs'].forEach(input =>{
        if (input['inputId']!= null){
            addMenuTextbox(formRoot, input);
        } else if (input['selectId'] != null) {
            addMenuDropdown(formRoot, input);
        } else if (input['buttonId'] != null) {
                addMenuButton(formRoot, input)
        } else {
            alert("JSON Error: Required properties missing from Menu Banner definition. See log for details." );
            console.error("JSON Error: Required properties missing from Menu Banner definition");
            console.error(input);
        }
    });

    if (!newItem.supressSubmit){
        addSubmitButton(formRoot, itemData);
    }

    newItem.appendChild(formRoot);
    menuRoot.appendChild(newItem);

}

function addMenuTextbox(formRoot, inputData){
    let newInput = document.createElement("input");
    if (inputData['inputType'] === 'passbox'){ newInput.type = "password"; }
    newInput.id = inputData['inputId'];
    newInput.name = inputData['inputId'];
    let cssClass = "NavInputText";
    if (inputData['cssClasses'] != null){
        inputData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }

    if (inputData['label'] != null) {
        newInput.placeholder = inputData['label'];
    }
    if (inputData['size'] != null) {
        newInput.size = inputData['size'];
    }
    newInput.className = cssClass;

    if (inputData['autoCallback'] != null){
        console.error("Warning: autoCallback support not yet implemented");
    }
    formRoot.appendChild(newInput);
}

function addMenuDropdown(formRoot, inputData){
    let newInput = document.createElement("select");
    newInput.id = inputData['selectId'];
    newInput.name = inputData['selectId'];
    let cssClass = "NavInputDD";
    if (inputData['cssClasses'] != null){
        inputData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }

    if (inputData.options == null || Array.isArray(!inputData.options) || inputData.options.length < 2){
        alert("JSON Error: Invalid or Missing Options List. See log for details.");
        console.error("Warning: DropDown element requires an array of at least 2 options");
        console.error(inputData);
        return;
    }
    if (inputData['size'] != null) {
        newInput.size = inputData['size'];
    }
    let optPrefix = inputData['label']??"";

    inputData.options.forEach(opt =>{
        let newOpt = document.createElement("option");
        newOpt.value=opt.value;
        newOpt.text = (optPrefix.length > 0?optPrefix + " ":"") + opt.text;
        if (opt.default){newOpt.selected = true;}
        newInput.appendChild(newOpt);
    });

    newInput.className = cssClass;

    if (inputData['autoCallback'] != null){
        console.error("Warning: autoCallback support not yet implemented");
    }

    formRoot.appendChild(newInput);
}


function addMenuBanner(menuRoot, bannerData){
    let cssClass = "NavItem";

    if (bannerData['cssClasses'] != null){
        bannerData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }
    let newItem = document.createElement('div');
    newItem.id = bannerData["bannerId"];
    newItem.className = cssClass;
    newItem.textContent = bannerData["label"];

    menuRoot.appendChild(newItem);
}

//[TODO] Do we really need this? Can't a banner with no text and no background do it just as well?
//Currently only used in billing/billing_navigation.  Investigate
function addMenuSpacer(menuRoot, spacerData){
    let cssClass = "NavItem";

    if (spacerData['cssClasses'] != null){
        spacerData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }
    let newItem = document.createElement('div');
    newItem.id = spacerData["spacerId"];
    newItem.className = cssClass;

    menuRoot.appendChild(newItem);
}

function addMenuButton(formRoot, buttonData){
    let newBtn = document.createElement("button");
    newBtn.id = buttonData.id;
    newBtn.type = "button";
    newBtn.textContent = buttonData.text;
    let cssClass = "NavItem";
    if (buttonData['cssClasses'] != null){
        buttonData['cssClasses'].forEach(c =>{ cssClass += (" " + c);}) //Append any secondary CSS classes
    }
    newBtn.className = cssClass;

    formRoot.appendChild(newBtn);
}

function addSubmitButton(formRoot, formData){
    let newBtn = document.createElement("button");
    newBtn.id = formRoot.id + "_submit";
    newBtn.type = "submit";
    newBtn.textContent = formData['submitText']??"Submit";
    newBtn.className = "NavItem";
    formRoot.appendChild(newBtn);
}
function ChangePage(page, target){
    if (target === "_top" || target == null){        
        window.top.location = page;
    } else {
		//[TODO] This works for iframes, not yet sure how well it works for other window/tabs. 
        let frm = window.document.getElementById(target);
        if (frm != null){
            frm.src = page;
        } else {
            alert ("Menu Error: Target Window/Frame '" + target + "' not found");
        }
    }
}

// Event functions
function HoverOn(){
    $(this).addClass('HoverItem');
}

function HoverOff(){
    $(this).removeClass('HoverItem');
}

function MenuClick(ev){
    ChangePage(this.url, this.target);
}