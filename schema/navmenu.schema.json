{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$note": "Change Schema URL for deployment",
    "$copyright": "Copyright 2022 Targren.com.",
    "$id": "/schema/navmenu.schema.json",    
    "title": "NavMenu",
    "description": "Navigation Menu Definition",
    "type": "object",
    "properties": {
        "menuId": { "description": "Unique Identifier for the menu", "type": "string" },
        "menuItems": {
            "description": "List of menuItem Objects displayed in the menu",
            "type": "array",
            "items": {
                "oneOf": [
                    {"$ref": "#/$defs/menuInput"},
                    {"$ref": "#/$defs/menuItem" },
                    {"$ref": "#/$defs/menuSpacer"},
                    {"$ref": "#/$defs/menuBanner"}
                ]
            },
            "minItems": 1,
            "uniqueItems": true
        }
    },
    "required": [ "menuId", "menuItems" ],
    "$defs" : {
        "menuItem": {
            "$id": "#/$defs/menuItem",
            "type": "object",
            "description": "An individual Navigation Menu entry",
            "properties": {
                "itemId": {"description": "id value for the menu item as a whole", "type":"string"},
                "label": {"type": "string", "description": "Text Displayed to user" },
                "target": {"type": "string", "description": "Target window/iframe id; omit or use '_top' to change current top window" },
                "url": {"type": "string", "description": "URL loaded by the menu item, or that input values are POSTed to" },
                "cssClasses": {"description": "Additional CSS classes to assign to the banner", "type": "array", "items": {"type": "string" } },
                "permissions": {"$ref": "#/$defs/userPermissions"}
            },
            "required": ["itemId", "label", "url"],            
            "query": {
                "oneOf": [
                    { "string": { "type": "string", "description": "Hard-coded query string"} },
                    { "vars":  {"type": "array", "description": "Name-Value pairs to build URI query string","items": {"type": "#/$defs/queryVar", "minItems": 1, "uniqueItems": true}}}
                ],
                "comment": "Note: Omit the '?' delimiter before the query string. It will be added programatically"
            }
        },
        "menuInput": {
            "$id": "#/$defs/menuInput",
            "type" : "object",
            "description": "A user input form for the menu, containing one or more input elements [NYI]",
            "properties": {
                "formId": {"description": "Unique Identifier for the form", "type": "string"},
                "submitText": {"description": "Label Text Shown on the 'submit' button for the form", "type": "string"},
                "supressSubmit": { "type":"boolean", "description": "If true, omit the submit button.", "_comment": "At least one element will need a callback setup, or the inputs won't do anything" },
                "url": {"type": "string", "description": "Target URL for form submission.", "_comment": "Can be blank (self-submit)"},
                "target": {"type": "string", "description": "Target window/iframe id; omit or use '_top' to change current top window" },
                "inputs": {
                    "type": "array",
                    "items": {"oneOf": [{"$ref": "#/$defs/formElementText"},{"$ref": "#/$defs/formElementDropdown"}]},
                    "minItems": 1,
                    "uniqueItems": true
                },
                "permissions": {"$ref": "#/$defs/userPermissions"}
            },
            "required": ["formId", "inputs", {"oneOf": ["url","supressSubmit"]}]
        },
        "menuBanner": {
            "$id": "#/$defs/menuBanner",
            "type": "object",
            "description": "Include a non-interactive text display on the menu bar [NYI]",
            "properties": {
                "bannerID": {"type": "string", "description": "Unique ID of the banner item"},
                "bannerText": {"type": "string", "description": "Text Content of the banner item"},
                "cssClasses": {"description": "Additional CSS classes to assign to the banner", "type": "array", "items": {"type": "string" } }
            },
            "required": ["bannerId"],
            "permissions": {"$ref": "#/$defs/userPermissions"}
        },
        "menuSpacer": {
            "$id": "#/$defs/menuSpacer",
            "description": "A blank div used as a spacer",
            "properties": {
                "spacerId": {"type": "string", "description": "Unique ID of the spacer item"},
                "cssClasses": {"description": "Additional CSS classes to assign to the spacer", "type": "array", "items": {"type": "string" } }
            },
            "required": ["spacerId"],
            "not" : {
                "required": ["permissions"]
            }
        },
        "formElementText": {
            "$id": "#/$defs/formElementText",
            "type": "object",
            "description": "A textbox",
            "required": ["inputId"],
            "properties": {
                "label": {"type": "string", "description": "Placeholder text for the text box"},
                "inputId": {"description": "The element id and also request field name (i.e. name used when GETting/POSTing)", "type": "string"},
                "size": {"type": "integer","description": "Determines the width of text boxes or the number of visible options in idle dropdown"},
                "cssClasses": {"description": "Additional CSS classes to assign to the control", "type": "array", "items": {"type": "string" } }
            }
        },
        "formElementDropdown": {
            "$id": "#/$defs/formElementDropdown",
            "type": "object",
            "description": "A select dropdown",
            "required": ["selectId", "options"],
            "properties": {
                "label": {"type": "string", "description": "Label for the input element. Prepends selection options for dropdowns"},
                "selectId": {"description": "The element id and also request field name (i.e. name used when GETting/POSTing)", "type": "string"},
                "options": {
                    "type": "array",
                    "items": {
                        "type": {
                            "value": {"type": "string"},
                            "text": {"type": "string"},
                            "default": {"type": "boolean"}
                        }
                    },
                    "minItems": 2,
                    "description": "Array of Value/Text options for dropdown. Ignored for other element types"
                },
                "size": {"type": "integer","description": "Determines the width of text boxes or the number of visible options in idle dropdown"},
                "multi": {"type": "boolean", "description": "Dropdown is a MultiSelect box"},
                "cssClasses": {"description": "Additional CSS classes to assign to the control", "type": "array", "items": {"type": "string" } }
            }
        },
        "formElementButton": {
            "$id": "#/$defs/formElementButton",
            "type": "object",
            "description": "A non-submit button. Requires an event handler to be defined in the html page script block",
            "required": ["buttonId"],
            "properties": {
                "label": {"type": "string", "description": "Label for the button."},
                "menuId": {"description": "The element id and also request field name (i.e. name used when GETting/POSTing)", "type": "string"},
                "cssClasses": {"description": "Additional CSS classes to assign to the control", "type": "array", "items": {"type": "string" } }
            }
        },
        "userPermissions": {
            "$id": "#/$defs/userPermissions",            
            "type": "object",
            "description": "Required user permission(s) to display the menu item.",
            "properties": {
                "perms": {"oneOf": [{
                    "type":"array",
                    "items": {"type": "string"},                    
                    "minItems": 1
                },{
                    "type":"string"
                }
                ]},
                "adminOnly": {"type": "boolean", "_comment": "Setting this boolean makes the menu item only appear for admin users"},
                "grouping": {
                    "type": "string",
                    "enum": ["any","all"],
                    "_comment": "If omitted or invalid, default to 'any'"
                }
            },
            "oneOf": [{"required":["perms"]}, {"required":["adminOnly"]}]
        },
        "queryVar": {
            "$id": "#/$defs/queryVar",
            "type": "object",
            "description": "Name-value pair to for URI query string",
            "name": {"type": "string", "description": "Query variable name"},
            "oneOf": [
                { "value":{"type": "string", "description": "Hard-coded value for variable"}},
                { "index": {"type": "string", "description": "Index of value stored in the global passVars object (must be populated in html page script block)"}}
            ]
        }
    }
}